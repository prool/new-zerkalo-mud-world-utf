# new-zerkalo-mud-world-utf

World (zones) for "New Zerkalo" MUD (multiusers dungeon) game
Cyrillic in UTF-8
Site of MUD http://zerkalo.kharkov.org

Это мир (игровые зоны и файлы данных) для мада "Новое Зеркало".
Кириллица в репозитории хранится в кодировке UTF-8, но при клонировании
репозитория автоматически перекодируется в стандартную для этого мада
кодировку koi8-r. Примерно так же, как в репозитории Былин на Гитхаб:
https://github.com/bylins/mud

/Prool
